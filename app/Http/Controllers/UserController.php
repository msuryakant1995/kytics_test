<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Authentication;
use Route;
use App\Master;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('user');
    }
    public function index()
    {
        $route = \Request::route()->getName(); 
        if(Master::checkurl($route) == 1)
        {
            return view('user.error');
        }
        else{
            return view('user.index');
        }
    }
}
