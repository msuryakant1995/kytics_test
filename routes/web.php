<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'admin'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/user-role','RoleController@index')->name('user-role');
    Route::get('/updaterole','RoleController@updaterole')->name('updaterole');
    Route::get('/page','PageController@index')->name('page');
    Route::post('/blockpage','PageController@store')->name('blockpage');
    Route::get('/unblockpage/{id}','PageController@unblock')->name('unblockpage');
});
Route::group(['middleware' => 'user'], function () {
    Route::get('/user', 'UserController@index')->name('user');
});

Route::get('/error','ErrorController@index');

