@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Block Pages') }}</div>

                <div class="card-body">
                <form method="POST" action="{{ route('blockpage') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Page Name') }}</label>

                            <div class="col-md-6">
                                <input id="page" type="text" class="form-control" name="page" required autocomplete="page" autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ADD') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="container">
                <div><h4>Blocked Pages</h4></div>
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Page Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    @php 
                    $count = 1;
                    @endphp
                    @foreach($pages as $page)
                    <tr>
                        <td>{{$count++}}</td>
                        <td>{{$page->route_name}}</td>
                        <td>
                        @if($page->status == 1)
                        Blocked
                        @else
                        Unblocked
                        @endif
                        </td>
                        <td>
                        @if($page->status == 1)
                        <a href="{{url('/unblockpage/'.$page->id)}}" class="badge badge-primary">Unblock</a>
                        @else
                        <a href="{{url('/unblockpage/'.$page->id)}}" class="badge badge-danger">Block</a>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
