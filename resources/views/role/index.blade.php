@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('User Lists') }}</div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Email ID</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        @php 
                        $count = 1;
                        @endphp
                        @foreach($user as $u)
                        <tr>
                            <td>{{$count++}}</td>
                            <td>{{$u->email}}</td>
                            <td>{{$u->email}}</td>
                            <td>
                                @if($u->role == 1)
                                <span id="role">Admin</span>
                                @elseif($u->role == 2)
                                <span id="role">User</span>
                                @endif
                            </td>
                            <td id="userStatus">
                            @if($u->role == 1)
                                <a href="{{url('/updaterole?id='.$u->id.'&role=2')}}" class="badge badge-warning">Mark As User</a>
                            @elseif($u->role == 2)
                                <a href="{{url('/updaterole?id='.$u->id.'&role=1')}}" class="badge badge-primary">Mark As Admin</a>
                            @endif
                                
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
