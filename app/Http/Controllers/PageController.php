<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Authentication;

class PageController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        $pages = Authentication::all();
        return view('page.index',['pages'=>$pages]);
    }
    public function store(Request $request)
    {
        $block = new Authentication();
        $block->route_name = $request->page;
        $block->status = 1;
        $block->save();
        return back();
    }
    public function unblock($id)
    {
        $unblock = Authentication::find($id);
        if($unblock->status == 1)
        {
            $unblock->status = 0;
        }
        else{
            $unblock->status = 1;
        }
        $unblock->save();
        return back();
    }
}
