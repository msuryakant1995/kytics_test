<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        $user = User::where('id','!=',\Auth::User()->id)->get();
        return view('role.index',['user'=>$user]);
    }
    public function updaterole(Request $request)
    {
        $user = User::find($request->id);
        $user->role = $request->role;
        $user->save();
        return back();
    }
}
