<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Authentication;

class Master extends Model
{
    //
    public static function checkurl($url)
    {
        $check = Authentication::where(['route_name'=>$url])->first();
        if(empty($check))
        {
            return 0;
        }
        else{
            if($check->status == 1)
            {
                return 1;
            }
            else {
                return 0;
            }
        }
    }
}
